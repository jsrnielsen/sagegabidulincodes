""" Class of Linearized Polynomials """
from sage.structure.element import AlgebraElement
from sage.structure.element import RingElement
from sage.algebras.algebra import Algebra
from sage.rings.ring import Ring
from sage.structure.unique_representation import UniqueRepresentation

class LinearizedPolynomial(RingElement):
   r"""
   Class for linearized polynomials
   
   INPUT:
   - ``parent`` - A ring
   - ``coeffs`` - List of coefficients
   
   FIELDS:
   - ``__coeffs`` - List of coefficients
   - ``__parent`` - INPUT parent

   """

   def __init__(self, parent, coeffs):
       RingElement.__init__(self, parent)
       Fqm = parent.base()
       self.__coeffs = [Fqm(c) for c in coeffs]
       self.normalize()
       self.__parent = parent

   def __nonzero__(self):
       """ Return if the polynomial is the zero polynomial """
       if len(self.__coeffs)==0:
           return False
       else:
           return True

   def _repr_(self):
       s = ""
       le = len(self.__coeffs)
       if le==0:
           return "(0)*x^(q^0)"
       else:
           for i in reversed(xrange(le)):
               if self.__coeffs[i]!=0:
                   if i != le-1:
                       s += " + "
                   s += "\n(%s)*x^(q^%s)"%(repr(self.__coeffs[i]),i) # Newline for better overview in output. TODO: Maybe remove it
           return s

   def coeffs(self):
       """ Return the list of coefficients """
       return self.__coeffs
   
   def coeff(self, i):
       """ Return the i-th coefficient of self. Coefficients outside [0,degree] are returnd as 0 """
       if i<0:
           return 0
       elif i>self.degree():
           return 0
       else:
           return self.__coeffs[i]

   def normalize(self):
       """ If the leading coefficient is 0, reduce the coefficient list size until leading coefficient is nonzero """
       le = len(self.__coeffs)
       for i in reversed(xrange(le)):
           if self.__coeffs[i] != 0:
               self.__coeffs = self.__coeffs[:(i+1)]
               return
       self.__coeffs = []
       return

   def degree(self):
       """ Return the (q-)degree of self """
       return len(self.__coeffs)-1
   
   def _eq_(self, other):
      """ Check for equality """
      return self.coeffs()==other.coeffs()

   def _neg_(self):
       """ Return additive inverse element of self """
       return LinearizedPolynomial(self.__parent, [-x for x in self.__coeffs])

   def _add_(self, other):
       """ Addition """
       x = self.__coeffs
       y = other.__coeffs
       if len(x)==len(y):
           new_coeffs = [x[i]+y[i] for i in xrange(len(x))]
       else:
           if(len(x)>len(y)):
               min = len(y)
               low = [x[i]+y[i] for i in xrange(min)]
               high = x[min:]
           else:
               min = len(x)
               low = [x[i]+y[i] for i in xrange(min)]
               high = y[min:]
           new_coeffs = low+high
       result = LinearizedPolynomial(self.__parent, new_coeffs)
       result.normalize()
       return result

   def _sub_(self, other):
       """ Subtraction """
       x = self.__coeffs
       y = other.__coeffs
       if len(x)==len(y):
           new_coeffs = [x[i]-y[i] for i in xrange(len(x))]
       else:
           if(len(x)>len(y)):
               min = len(y)
               low = [x[i]-y[i] for i in xrange(min)]
               high = x[min:]
               new_coeffs = low+high
           else:
               min = len(x)
               low = [x[i]-y[i] for i in xrange(min)]
               high = [-y[i] for i in xrange(min,len(y))]
               new_coeffs = low+high
       result = LinearizedPolynomial(self.__parent, new_coeffs)
       result.normalize()
       return result

   def _mul_(self, other):
       """ Multiplication """
       q = self.__parent.q
       a = self.__coeffs
       F = self.__parent.base()
       d_a = self.degree()
       b = other.__coeffs
       d_b = other.degree()
       c = [F(0) for i in xrange(d_a+d_b+1)]
       for j in xrange(d_a+d_b+1):
           for i in xrange(j+1):
               if i<=d_a and j-i<=d_b:
                   c[j] += a[i]*(b[j-i]^(q^i))
       result = LinearizedPolynomial(self.__parent, c)
       result.normalize()
       return result
   
   def leading_coefficient(self):
      """ Return the leading coefficient of self. TODO: Maybe rename the function """
      return self.__coeffs[self.degree()]
       
   def evaluate(self, x):
       """ Evaluate the lin. polynomial self at the position x """
       q = self.__parent.q
       a = self.__coeffs
       F = self.__parent.base()
       if not x in F:
           raise ValueError("x must be in %s"%F)
       result = F(0)
       for i in xrange(self.degree()+1):
           result += F(a[i]*x^(q^i))
       return result

   def right_div(self, other, debug=False):
       """ Right division as Algorithm 2.1 [W13].
       TODO: Make it faster by directly working on the internal representation (as coefficient list)
       instad of creating a lot of temporary new linearized polynomials. """
       if debug:
           print "** right_div START"
       if other.degree()==-1:
           raise ZeroDivisionError("other polynomial must be nonzero")
       R = self.__parent
       q = R.q
       F = R.base()
       #print F
       a = self
       b = other
       d_a = a.degree()
       d_b = b.degree()
       p = LinearizedPolynomial(R, [])
       if d_a<d_b:
           r = self
       else:
           while d_a>=d_b:
               l = [0 for i in xrange(d_a-d_b+1)]
               l.insert(d_a-d_b, 1)
               monomial = LinearizedPolynomial(R, l)
               #p_tmp = LinearizedPolynomial(R, [a.__coeffs[d_a]/(b.__coeffs[d_b]^(q^(d_a-d_b)))])*monomial
               p_tmp = a.__coeffs[d_a]/(b.__coeffs[d_b]^(q^(d_a-d_b)))*monomial
               a = a-p_tmp*b
               d_a = a.degree()
               p += p_tmp
           r = a
       if debug:
           print "** right_div END"
       return [p, r]

   def left_div(self, other, debug=False):
      """ Left division as Algorithm 2.2 [W13]
      TODO: Same as for right_div and implement a faster "nth-root-function" """
      if debug:
         print "** left_div START"
      if other.degree()==-1:
           raise ZeroDivisionError("other polynomial must be nonzero")
      R = self.__parent
      q = R.q
      F = R.base()
      #print F
      a = self
      b = other
      d_a = a.degree()
      d_b = b.degree()
      p = LinearizedPolynomial(R, [])
      if d_a<d_b:
          r = self
      else:
          while d_a>=d_b:
              l = [0 for i in xrange(d_a-d_b+1)]
              l.insert(d_a-d_b, 1)
              monomial = LinearizedPolynomial(R, l)
              # TODO: implement faster "nth-root-function"
              #coeff = (a.__coeffs[d_a]/b.__coeffs[d_b])^(self.__parent.Zqm(q^-d_b))#.nth_root(q^d_b)
              coeff = (a.__coeffs[d_a]/b.__coeffs[d_b])^(q^(self.__parent.m-d_b))
              p_tmp = coeff*monomial
              a = a-b*p_tmp
              d_a = a.degree()
              p += p_tmp
          r = a
      if debug:
         print "** left_div END"
      return [p, r]

   def right_LEEA(self, other, d, debug=False):
      """ right linearized extended euclidean algorithm as Algorithm 2.3 [W13].
      TODO: Same as for right_div and remove debug parts at some point """
      if debug:
          print "** right_LEEA START"
      R = self.__parent
      if self.degree()>=other.degree():
         a = self
         b = other
      else:
         raise ValueError("degree of a must be at least deg(b))")
      rdd = a
      rd  = b
      udd = LinearizedPolynomial(R, [0])
      ud  = LinearizedPolynomial(R, [1])
      vdd = LinearizedPolynomial(R, [1])
      vd  = LinearizedPolynomial(R, [0])
      while rd.degree()>=d:
         [p, r] = rdd.right_div(rd, debug)
         u = udd-p*ud
         v = vdd-p*vd
         rdd = rd
         rd  = r
         udd = ud
         ud  = u
         vdd = vd
         vd  = v
      if debug:
          print "** right_LEEA END"
      return [rd, ud, vd]

   def make_monic(self):
       """ Make self monic (i.e. divide all coefficients by the leading coefficient """
       a = self.leading_coefficient()
       result = [c/a for c in self.__coeffs]
       return LinearizedPolynomial(self.__parent, result)
       


class LinearizedPolynomialRing(UniqueRepresentation, Ring):
    Element = LinearizedPolynomial
    
    def __init__(self, base, q):
        if not base in Fields():
            raise ValueError("base must be a field")
        Ring.__init__(self,base)
        self.q = q
        self.m = base.order().log(q)

    def _repr_(self):
        return "LinearizedPolynomialRing(%s)"%repr(self.base())

    def is_commutative(self):
        return False

    def gen(self):
        return LinearizedPolynomial(self,[0,1])
           

    def random_element(self, degree=2):
        if isinstance(degree, (list, tuple)):
            if len(degree) != 2:
                raise ValueError("degree argument must be an integer or a tuple of 2 integers (min_degree, max_degree)")
            if degree[0] > degree[1]:
                raise ValueError("minimum degree must be less or equal than maximum degree")
            degree = randint(*degree)
        R = self.base_ring()
        result = LinearizedPolynomial(self, ([R.random_element() for _ in xrange(degree+1)]))
        while result.degree()<degree:
            result = LinearizedPolynomial(self, ([R.random_element() for _ in xrange(degree+1)]))
        return result
    
    def _element_constructor_(self, *args, **kwds):
        if len(args)!=1:
            raise ValueError("number of arguments must be 1")
        x = args[0]
        if x in self.base():
            return self.element_class(self, [x])
        else:
            return self.element_class(self, x)

    def _coerce_map_from_(self, S):
        if self.base().has_coerce_map_from(S):
            return True
        else:
            return False



"""
BIBLIOGRAPHY

[W13] Antonia Wachter-Zeh,
      Decoding of Block and Convolutional Codes in Rank Metric,
      Dissertation, Ulm University, 2013

"""
