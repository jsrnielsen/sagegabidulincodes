""" Class for representing Gabidulin codes and some useful functions for encoding and decoding """
load linearized_polynomials.sage
load maps.sage
load helper.sage
from codinglib import *

class Gab(BlockCodeAbstract):
    r"""
    Class for Gabidulin Codes
    
    INPUT:
    - ``Fqm`` - Univariate Quotient Polynomial Ring over Fq with modulus being some
    irreducible polynomial of degree m. Hence, it behaves like a galois field of size q^m, but
    the advantage compared to a finite field of size q^m is that is has a coercion mapping from Fq.
    Disadvantage: It does not provide a fast "nth_root" function which is needed by some functions.
    TODO: Implement a fast function which finds the q^i-th root of an Fqm element.
    - ``Fq``  - Finite Field of size q (prime power)
    - ``L``   - Linearized Polynomial Ring with coefficients in Fqm and Frobenius-q-automorphism
    - ``n``   - Code length
    - ``k``   - Code dimension
    - ``gs``  - Vector over Fqm containing n elements from Fqm which are linearly independent over Fq
    
    FIELDS:
    - ``Fqm`` - Same as INPUT
    - ``Fq``  - Same as INPUT
    - ``L``   - Same as INPUT
    - ``n``   - Same as INPUT
    - ``k``   - Same as INPUT
    - ``gs``  - Same as INPUT
    - ``q``   - Size of Fq
    - ``m``   - Extension degree [Fqm:Fq]
    - ``d``   - Minimum rank distance
    - ``Vinv``  - Inverse of the q-Vandermonde matrix used to speed up lagrange interpolation (does not have to be set after __init__)
    - ``Vinv_set`` - Flag indicating whether Vinv was already set
    - ``G``     - Generator matrix (does not have to be set after __init__)
    - ``G_set`` - Flag indicating whether G was already set
    - ``H``     - Parity check matrix (does not have to be set after __init__)
    - ``H_set`` - Flag indicating whether H was already set
    - ``debug`` - Flag used for debugging
    """
    
    def __init__(self, Fqm, Fq, L, n, k, gs):
        # Check consistency of the input variables and determine some other variables
        p = Fq.characteristic()
        q = Fq.order()
        assert n==gs.degree(), "Number of g's not equal to the codelength"
        assert ext(Fqm, Fq, gs).rank()==n, "g's are not linearly independent over Fq"
        # TODO: If the ext mapping works properly, the following function is redundant to the previous line,
        # but a lot slower. Remove it at some time.
        #assert check_if_basis(gs, q, Fqm), "gs is not a basis of Fqm over Fq"
        assert k>0 and k<n, "Dimension k must be 0<k<n"
        assert p==Fqm.characteristic(), "Fqm and Fq must have the same characteristic"
        m = Fqm.order().log(q)
        assert m in ZZ, "Fqm must be a field extension of Fq"
        assert n<=m, "n must be <=m"

        # Helper fields
        self.debug = False
        
        # Create fields which are set here
        self.Fqm = Fqm
        self.Fq  = Fq
        self.L   = L
        self.n   = n
        self.k   = k
        self.gs  = gs
        self.q   = q
        self.m   = m
        self.d   = n-k+1

        # Create fields which are set later
        self.Vinv  = self.create_Vinv()
        self.Vinv_set = 1
        self.G     = 0
        self.G_set = 0
        self.H     = 0
        self.H_set = 0

    def __str__(self):
        return "[%s,%s,%s] Gabidulin code over %s"%(self.n, self.k, self.d, latex(self.Fqm))
    
    def __repr__(self):
        return self.__str__()
    
    def _latex_(self):
        return "[%s,%s,%s]{\rm\ Gabidulin\ code\ over\ }%s"%(self.n, self.k, self.d, latex(self.Fqm))
    
    def __eq__(self, other):
        """ Check for total equivalence of Gabidulin codewords """
        return isinstance(other, Gab) \
           and self.Fqm == other.Fqm  \
           and self.Fq  == other.Fq   \
           and self.L   == other.L    \
           and self.q   == other.q    \
           and self.m   == other.m    \
           and self.n   == other.n    \
           and self.k   == other.k    \
           and self.d   == other.d    \
           and self.gs  == other.gs
    
    def generator_matrix(self):
        """ Return the generator matrix corresponding to the elements of gs """
        return matrix(self.Fqm, self.k, self.n,
                      lambda i,j: self.gs[j]^(self.q^i))
    
    def parity_check_matrix(self):
        """ Return the parity check matrix corresponding to the elements of gs """
        q = self.q
        m = self.m
        n = self.n
        k = self.k
        # solve system of linear equations (cf. Eq. (2.28) [W13]):
        A = matrix(self.Fqm, n-1, n-1)
        b = vector(self.Fqm, n-1)
        for j in xrange(-n+k+1, k):
            for i in xrange(n-1):
                if j<0:
                    tmp = gs[i]^(q^(m+j))
                else:
                    tmp = gs[i]^(q^j)
                A[j+n-k-1,i] = tmp
            if j<0:
                b[j+n-k-1] = -gs[n-1]^(q^(m+j))
            else:
                b[j+n-k-1] = -gs[n-1]^(q^j)
        c = A.solve_right(b)
        h = vector(self.Fqm, n)
        for i in xrange(n-1):
            h[i] = c[i]
        h[n-1] = 1
        return matrix(self.Fqm, self.n-self.k, self.n,
                      lambda i,j: h[j]^(self.q^i))
    
    def create_Vinv(self):
        """ Creates the inverse of the q-Vandermonde matrix used to speed up lagrange interpolation """
        if self.debug:
            print "** create_Vinv START"
        V = matrix(self.Fqm, self.n, self.n, lambda i,j: self.gs[i]^(self.q^j))
        if self.debug:
            print "** create_Vinv END"
        return V.inverse()
        
    def encode(self, info):
        """ Encode the information given by the linearized polynomial info of q-degree <k """
        assert info in self.L, "Information should be a linearized polynomial over %s"%self.Fqm
        assert info.degree()<self.k, "Information polynomial must have qdeg<k"
        c = vector(Fqm, n)
        for i in xrange(n):
            c[i] = info.evaluate(self.gs[i])
        return c

    def syndrome(self, r):
        """ Compute the syndrome """
        return r*(self.H.transpose())

    def syndrome_matrix(self, s, t):
        """ Compute the syndrome matrix of dimension t times t """
        S = matrix(self.Fqm, t, t)
        b = vector(self.Fqm, t)
        for i in xrange(t):
            for j in xrange(t):
                S[i,j] = s[t+i-j]^(q^j)
            b[i] = -s[i]^(q^t)
        return [S, b]

    def decode2(self, r):
        """ Decode like Algorithm 3.5 [W13] (not finished!! => TODO) """
        s = self.syndrome(r)
        if s==0:
            return r
        else:
            t = floor((self.n-self.k)/2)
            [S, b] = self.syndrome_matrix(s,t)
            while S.rank()<t:
                t -= 1
                [S, b] = self.syndrome_matrix(s,t)
            c = S.solve_right(b)
            u = vector(self.Fqm, t+1)
            u[0:t] = c
            u[t] = 1
            # TODO: continue!!
            return u
    
    def random_rank_error(self, t):
        """ Return a random rank error of rank t.
        TODO: Is the result uniformly distributed among all possible rank-t-errors? """
        R1 = random_matrix(self.Fq, self.m, t)
        while R1.rank()<t:
            R1 = random_matrix(self.Fq, self.m, t)
        R2 = random_matrix(self.Fq, t, self.n)
        while R2.rank()<t:
            R2 = random_matrix(self.Fq, t, self.n)
        return [R1*R2,R1,R2]
        
    def linearized_lagrange_basis_polynomial(self, i):
        """ Compute the i-th linearized Lagrange basis polynomial of q-degree n-1 (cf. Eq. (3.24) [W13]).
        TODO: I think this is terribly slow. Find a more elegant way! """
        return minimal_subspace_polynomial(self.L, self.gs, i)
    
    def lagrange_interpolation(self, r):
        """ Apply lagrange interpolation to find the unique linearized polynomial of q-degree <n
        such that rr(gs[i])=r(i) for all i. TODO: Can this be implemented faster? """
        rr = LinearizedPolynomial(self.L, [0])
        for i in xrange(self.n):
            L = self.linearized_lagrange_basis_polynomial(i)
            rr = rr._add_(LinearizedPolynomial(self.L, [r[i]/(L.evaluate(gs[i]))])._mul_(L))
        return rr

    def lagrange_interpolation_faster(self, r):
        """ Faster (?) lagrange interpolation. TODO: Find even faster version or use q-transform """
        if self.Vinv_set==0:
            self.Vinv = self.create_Vinv()
            self.Vinv_set = 1
        return LinearizedPolynomial(self.L, self.Vinv*r)

    def minimal_subspace_polynomial_G(self):
        """ Compute the minimal subspace polynomial of G = {g_1, ..., g_n} """
        n = self.n
        m = self.m
        # If n==m, the min. subspace polynomial becomes x^[n]-x^[0] (cf. [W13], remark following Thm. 3.6)
        if m==n:
            l = [0 for i in xrange(n+1)]
            l.insert(0,-1)
            l.insert(n,1)
            return LinearizedPolynomial(self.L, l)
        else:
            return minimal_subspace_polynomial(self.L, self.gs)    
    
    def decode_gao_like(self, r, info, debug=False):
        """ Decode Gao-like (cf. e.g. Algorithm 3.6 [W13]).
        TODO: Remove the debug parts when the function is tested better """
        if self.debug:
            print "* decode_gao_like START"
        rr = self.lagrange_interpolation_faster(r)
        if debug:
            print "\n--------------------------------------------------"
            print "Check if lagrange_interpolation worked:"
            for i in xrange(self.n):
                if rr.evaluate(gs[i])==r[i]:
                    print "yes"
                else:
                    print "no"
                    print "rr.degree() =", rr.degree(), " ( n =", self.n, ")"
                    print "--------------------------------------------------\n"
        M = self.minimal_subspace_polynomial_G()
        dstop = floor((self.n+self.k)/2)
        [rout, uout, vout] = M.right_LEEA(rr, dstop, self.debug)
        if debug:
            #print "\nrr    = ", rr, "\n"
            #print "M     = ", M, "\n"
            print "\n--------------------------------------------------"
            print "Check if right_LEEA worked:"
            print "rout == vout*M+uout*rr:    ", rout._eq_(vout._mul_(M)+uout._mul_(rr))
            print "deg(rout)<floor((n+k)/2):  ", rout.degree()<dstop
            print "deg(uout)<=floor((n-k)/2): ", uout.degree()<=floor((self.n-self.k)/2)
            print "rout == uout*info:         ", rout._eq_(uout._mul_(info))
            print "deg(rout) = ", rout.degree()
            print "deg(uout) = ", uout.degree()
            print "deg(rr  ) = ", rr.degree()
            print "--------------------------------------------------\n"
            #if rout._eq_(vout*M+uout*rr):
            #    print "ORANGE"
            #else:
            #    print "KIWI"
            #if rout.degree()<dstop:
            #    print "A"
            #else:
            #    print "B"
            #print "rout  = ", rout, "\n"
            print "uout  = ", uout, "\n"
            #print "vout  = ", vout, "\n"
        [f,rest] = rout.left_div(uout, self.debug)
        if debug:
            print "--------------------------------------------------"
            print "Check if left_div worked:"
            print "rout==uout*f+rest:   ", rout._eq_(uout._mul_(f)+rest)
            print "--------------------------------------------------\n"
        if self.debug:
            print "* decode_gao_like END"
        return [f, rest, rout, uout, vout]
        
        




"""
Testing Code
"""


###############################################################################
# Parameters

q = 3^4
m = 10
k = 3
debug_local = False



###############################################################################
# More Variables

n = m
Fq.<a> = GF(q)
Fqm.<b> = GF(q^m)
#
# Construction of a "field tower" with coercion, see e.g.
# http://ask.sagemath.org/question/9604/explicit-finite-field-extensions/
# (TODO: find simpler solution!)
#
PF.<t> = Fq[]
poly_list = prime_factors(PF(b.minpoly()))
poly_q_to_qm = poly_list[0]
Fqm.<c> = Fq.extension(poly_q_to_qm)



###############################################################################
# Initialize gs 

GS = matrix(Fq, m, n)
for i in xrange(n):
    GS[i,i] = 1
gs = ext_inv(Fqm, Fq, GS)



###############################################################################
# Create Linearized Polynomial Ring & Gabidulin Code

L = LinearizedPolynomialRing(Fqm, q)
G = Gab(Fqm, Fq, L, n, k, gs)



###############################################################################
# Encode

info = L.random_element(k-1)

print "\n--------------------------------------------------"
print "Random Information Word"
print info, "\n"
c = G.encode(info)
C = ext(Fqm, Fq, c)



###############################################################################
# Error

num_errors = floor((n-k)/2)
[E,A_m,B_m] = G.random_rank_error(num_errors)

# Real Error Span Polynomial for debugging
a_m = ext_inv(Fqm, Fq, A_m)
esp = minimal_subspace_polynomial(L, a_m)

# Display E
print "\n--------------------------------------------------"
print "Error Matrix\n"
print "Half the Min. Rank Dist. = ", floor((m-k)/2)
print "Number of Errors         = ", E.rank(), "\n"

# Compute received word
R = C+E
r = ext_inv(Fqm, Fq, R)



###############################################################################
# Decode

[f,rest,rout,uout,vout] = G.decode_gao_like(r, info, debug_local)

# Display Results
print "\n--------------------------------------------------"
print "Results\n"
print "f = ", f, "\n"
print "rest = ", rest, "\n"

print "\n--------------------------------------------------"
if f._eq_(info):
    print "DECODING SUCCESSFUL!\n\n"
else:
    print "DECODING FAILURE!\n\n"


###







"""
BIBLIOGRAPHY

[W13] Antonia Wachter-Zeh,
      Decoding of Block and Convolutional Codes in Rank Metric,
      Dissertation, Ulm University, 2013

"""






















"""
Code which is sometimes used (TODO: Remove at some point).



print "\n--------------------------------------------------"

print err_loc_real
uout_n = (uout.coeff(uout.degree()))^(-1)*uout
print uout
print uout_n
#print err_loc_real*err_loc_real
[qn,rn] = uout.right_div(err_loc_real)
print "qn = ", qn, "\n"
print "rn = ", rn, "\n"


[qnn,rnn] = rout.left_div(qn)
print "qnn = ", qnn, "\n"
print "rnn = ", rnn, "\n"



print "\n--------------------------------------------------"

# Lagrange Interpolation
rr = G.lagrange_interpolation(r)
M = minimal_subspace_polynomial(L, gs)

apfel = err_loc_real*(rr-info)
[qapfel, rapfel] = apfel.left_div(M)
banane = uout*(rr-info)

print a_m
for i in xrange(n):
    print i, ":", apfel.evaluate(gs[i])==0
    #print rr.evaluate(gs[i])
    #print info.evaluate(gs[i])
for i in xrange(n):
    print i, ":", banane.evaluate(gs[i])==0

#print "qapfel = ", qapfel, "\n"
#print "rapfel = ", rapfel, "\n"


#for i in xrange(a_m.degree()):
#    print err_loc_real.evaluate(a_m[i])




#e = ext_inv(Fqm, Fq, E)
#for i in xrange(n):
#    print i, ":", err_loc_real.evaluate(e[i])==0


print "\n--------------------------------------------------"


print ext(Fqm, Fq, ext_inv(Fqm, Fq, E))==E


"""

"""
print "\n--------------------------------------------------"

u = G.decode_classically(r)
print "syndrome decoder result: "
for i in u:
    print i

f = uout
g = uout.make_monic()
a = g.coeffs()

print ""
for i in xrange(a.__len__()):
    print a[i]==u[i]

print "\n--------------------------------------------------"

#for i in xrange(n):
#    print uout.evaluate(gs[i])==0
"""




