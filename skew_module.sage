r"""Functionality for free F[x; theta]-modules, i.e. modules over skew polynomial rings.
In particular for calculating bases in weak Popov form. The weak Popov form is
slightly stronger than row reduced form, and it is also a type of reduced
Groebner bases over some simple module monomial orderings.

The forms here are all *left* forms, in the sense that left row operations are
applied.
"""

from codinglib.util import *

def LP(v, weights=None):
    """If v is a vector of polynomials, return the leading position of v using
    <_w where w is the weights vector (non-negative integers, 0 is assumed as
    all weights if none given). In case of tie, the highest position is given"""
    #TODO: there might be a bug in this function when weights are used
    if not weights:
        best=-1
        bestp=-1
        for p in range(0,len(v)):
            vpdeg = v[p].degree()
            if vpdeg >= best:
                best=vpdeg
                bestp = p
        if best==-1:
            return -1
        else:
            return bestp
    else:
        best=-1
        bestp=-1
        for p in range(0,len(v)):
            if not v[p].is_zero():
                vpdeg = v[p].degree() + weights[p]
                if vpdeg >= best:
                    best=vpdeg
                    bestp = p
        if best==-1:
            return -1
        else:
            return bestp

def skew_module_row_reduction(M, i, j, pos):
    """Perform a row reduction with row j on row i, reducing position
    pos. If M[i,pos] has lower degree than M[j,pos], nothing is changed.
    Returns the multiple of row j used."""
    q = M.base_ring().q
    pow = M[i,pos].degree() - M[j,pos].degree()
    if pow < 0:
        return None
    coeff = -M[i, pos].leading_coefficient() / M[j, pos].leading_coefficient()^(q^pow)
    x = M.base_ring().gen()
    multiple = coeff*x^pow
    M.add_multiple_of_row(i, j, multiple)
    return coeff*x^pow

def skew_module_mulders_storjohann(M, weights=None, debug=0):
    """Reduce $M$ to weak Popov form using the Mulders--Storjohann
    algorithm (Mulders, T., and A. Storjohann. "On Lattice Reduction
    for Polynomial Matrices." Journal of Symbolic Computation 35, no.
    4 (2003): 377-401.).
    If debug is True, then print some info."""
    # initialise conflicts list and LP map
    LP_to_row = dict( (i,[]) for i in range(M.ncols()))
    conflicts = []
    for i in range(M.nrows()):
        lp = LP(M.row(i), weights=weights)
        ls = LP_to_row[lp]
        ls.append(i)
        if len(ls) > 1:
            conflicts.append(lp)
    iters = 0
    # while there is a conflict, do a row reduction
    while conflicts:
        lp = conflicts.pop()
        ls = LP_to_row[lp]
        i, j = ls.pop(), ls.pop()
        if M[i,lp].degree() < M[j, lp].degree():
            j,i = i,j

        skew_module_row_reduction(M, i, j, lp)
        ls.append(j)
        lp_new = LP(M.row(i), weights=weights)
        if lp_new > -1:
            ls_new = LP_to_row[lp_new]
            ls_new.append(i)
            if len(ls_new) > 1:
                conflicts.append(lp_new)
        iters += 1
    return iters

def skew_module_weak_popov(M, debug=0):
    """Compute a weak Popov form of $M$."""
    return skew_module_mulders_storjohann(M, debug)

def skew_module_is_weak_popov(M):
    """Return whether the matrix is in weak Popov form"""
    seen = set()
    for i in range(M.nrows()):
        lp = LP(M.row(i))
        if lp in seen:
            return False
        seen.add(lp)
    return True

def skew_module_degree(M):
    """Return the maximal degree occurring in M."""
    return max( M[i,j].degree() for i in range(M.nrows()) for j in range(M.ncols()))

def skew_module_all_degrees(M, weights=None):
    """Return a matrix containing the degrees of all the entries of M"""
    if not weights:
        return matrix(M.nrows(), M.ncols(), lambda i,j: M[i,j].degree())
    else:
        return matrix(M.nrows(), M.ncols(), lambda i,j: M[i,j].degree() + weights[j] if not M[i,j].is_zero() else -1)

def skew_module_degree_of_row(M, i, weights=None):
    """Return the degree of the i'th row of M, i.e. the greatest degree among
    its elements."""
    if weights is None:
        return max(poly_degs(M.row(i)))
    else:
        r = M.row(i)
        return max(r[j].degree() + weights[j]  if not r[j].is_zero() else -1 for j in range(M.ncols()))

def skew_module_row_degree(M):
    """Return the row-degree of $M$, i.e. the sum of all rows' degrees"""
    return sum( skew_module_degree_of_row(M, i) for i in range(0, M.nrows()) )

def skew_module_orthogonality_defect(M):
    """Return the orthogonality defect of the square matrix $M$ over a
    polynomial ring. Requires the calculation of the determinant of $M$."""
    raise NotImplementedError
    return skew_module_row_degree(M) - M.determinant().degree()

def skew_module_is_popov(M):
    """Returns true if M is in Popov form, otherwise throws an Assertion
    error"""
    nrows, ncols = M.nrows(), M.ncols()
    lpos = [ LP(M.row(i)) for i in range(0, nrows) ]
    ldegs = [ M[i,lpos[i]].degree() for i in range(0,nrows) ]
    for i in range(1, nrows):
        mypos = lpos[i]
        assert(M[i,mypos].leading_coefficient().is_one())
        assert(ldegs[i] > ldegs[i-1] or (ldegs[i] == ldegs[i-1] and mypos > lpos[i-1]))
        for j in range(0, ncols):
            if j!=i:
                assert(M[i, mypos].degree() > M[j, mypos].degree())
    return True

def skew_module_popov(M):
    """Reduce M to the unique Popov form of M and return the permutation matrix
    needed for achieving Popov form from weak Popov form."""
    raise NotImplementedError
    skew_module_weak_popov(M)
    nrows = M.nrows()
    #Make monic
    lpos = [ LP(M.row(i), None) for i in range(0, nrows) ]
    for i in range(0,nrows):
        M.set_row_to_multiple_of_row(i,i,1/M[i,lpos[i]].leading_coefficient())
    #Bubble sort the rows by degrees, tie breaking with lpos
    #TODO: This should be done much faster: precompute where rows
    #should go (w. fast sorting), to minimise no. of swaps in the
    #matrix
    perm = copy(M.parent().one())
    ldegs = [ M[i,lpos[i]].degree() for i in range(0,nrows) ]
    for i in range(1, nrows):
        cur = i
        while cur > 0 and (ldegs[cur] < ldegs[cur-1] or (ldegs[cur] == ldegs[cur-1] and lpos[cur] < lpos[cur-1])):
            M.swap_rows(cur, cur-1)
            perm.swap_rows(cur, cur-1)
            ldegs[cur], ldegs[cur-1] = ldegs[cur-1], ldegs[cur]
            lpos[cur], lpos[cur-1] = lpos[cur-1], lpos[cur]
            cur -= 1
    #Perform reductions with earlier rows on later ones, in the right order
    rowMap = dict()
    for i in range(0, nrows):
        getPos = lambda: max([(pos, M[i,pos].degree()-ldegs[rowMap[pos]])
                                    for pos in rowMap.keys()]+[(-1,-1)],
                               key=lambda(p,d): d)
        (pos, diff) = getPos()
        while diff>=0:
            skew_module_row_reduction(M,i,rowMap[pos],pos)
            (pos, diff) = getPos()
        rowMap[lpos[i]] = i
    # Make sure everything is all right
    assert(skew_module_is_popov(M))
    return perm     

def skew_module_construct_vector(Mp, v):
    """If the polynomial vector v lies in Mp, where Mp is a module by
    rows, and is in weak Popov form, then return the F[x]-linear combination of
    the rows of Mp which create v. This is essentially the multivariate division
    algorithm."""
    raise NotImplementedError
    rowMap = dict([ (LP(Mp.row(i)) , i) for i in range(0,Mp.nrows()) ])
    combs = [Mp.base_ring().zero() for t in range(0, Mp.nrows())]
    x = Mp.base_ring().gen()
    while not v.is_zero():
        pos = LP(v)
        redPoly = Mp[rowMap[pos], pos]
        pow = v[pos].degree() - redPoly.degree()
        if pow < 0:
            return None
        coeff = v[pos].leading_coefficient() / redPoly.leading_coefficient()
        multiple = coeff * x^pow
        v = v - multiple*Mp.row(rowMap[pos])
        combs[rowMap[pos]] += multiple
    return combs

def skew_module_contains(Mp, v):
    """Test whether the polynomial vector v lies in Mp, where Mp is a module by
    rows, and is in weak Popov form."""
    return skew_module_construct_vector(Mp, v) != None

def skew_module_subset_module(M1, M2):
    """Test whether M2 is a subset of M1 as modules by rows, by transforming a
    copy of M1 to popov form and asserting that the bases of M2 are in this."""
    M1c = copy(M1)
    skew_module_weak_popov(M1c)
    for i in range(0, M2.nrows()):
        if not skew_module_contains(M1c, M2.row(i)):
            print "Row %s failed" % i
            return False
    return True

def skew_module_apply_weights(M, weights):
    """Multiply inplace the $n$th column with $x^{w_n}$ where $w$ is the weights"""
    for j in range(M.ncols()):
        M.set_col_to_multiple_of_col(j,j, x^weights[j])

def skew_module_remove_weights(M, weights):
    """Multiply inplace the $n$th column with $x^{-w_n}$ where $w$ is the weights"""
    for i in range(M.nrows()):
        for j in range(M.ncols()):
            M[i,j] = M[i,j].shift(-weights[j])

def skew_module_row_LP(M, lp, weights=None):
    """Return the index of the (first) row with the given leading
    position. Throws an error if there is no such row."""
    i=0
    while LP(M.row(i), weights=weights) != lp:
        i=i+1
    return i
    
def skew_module_minimal_row(M, weights=None):
    """Return the (least) index of the (non-zero) row with minimal degree"""
    (best,besti) = skew_module_degree_of_row(M, 0, weights), 0
    for i in range(1, M.nrows()):
        d = skew_module_degree_of_row(M, i, weights)
        if d > -1 and d < best:
            (best,besti) = d, i
    return besti
            
