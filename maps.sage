""" Functions for ext: Fqm -> Fq^m and ext^(-1): Fq^m -> Fqm mappings, i.e. the bijection between F_(q^m) and (F_q)^m """

def ext_single(Fqm, Fq, x, debug=False):
    """ ext(x) for a singe element from Fqm """
    
    # check for consistency
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    # Some old code which might be used again:
    """
    # compute result
    if q==p: # if q=p prime, the mapping is easy
        if debug:
            print "----------------------------------------------"
            print "ext:"
            print x
            print x._vector_()
        return x._vector_()
    else:    # else, there has to be a detour over Fp
        return vector(Fq, x.list())
    """
    if q==p:
        return x._vector_()
    else:
        return vector(Fq, x.list())


def ext_inv_single(Fqm, Fq, x, debug=False):
    """ ext^(-1)(x) for a single x from Fq^m """
    
    # check for consistency
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    # Some old code which might be used again:
    """
    # compute result
    result = Fqm(0)
    gen = Fqm.gen()
    if q==p: # if q=p prime, the mapping is easy
        for i in xrange(m):
            result += x[i,0]*gen^i
        if debug:
            print "----------------------------------------------"
            print "ext_inv:"
            print x.str()
            print result
        return result
    else:
        #assert Fqm.degree()==x.degree(), "x must have the same dimension as [Fqm:Fq]"
        result = Fqm(0)
        gen = Fqm.gen()
        for i in xrange(Fqm.degree()):
            result += x[i,0]*gen^i
        return result
    """
    result = Fqm(0)
    gen = Fqm.gen()
    for i in xrange(Fqm.degree()):
        result += x[i,0]*gen^i
    return result


def ext(Fqm, Fq, a, debug=False):
    """ ext for a l-dimensional vector over Fqm. Returns an mxl matrix over Fq """
    
    # check for consistency
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    
    # compute result
    if a in Fqm:
        A = Matrix(Fq, m, 1)
        A[:,0] = map_Fqm_to_Fq(Fqm, Fq, a, debug)
        return A
    else:
        n = a.degree()
        A = Matrix(Fq, m, n)
        for i in xrange(n):
            #A[:,i] = map_field_to_vector(Fqm, Fq, a[0,i])
            #A[:,i] = map_Fqm_to_Fq(Fqm, Fq, a[i], debug)
            A[:,i] = ext_single(Fqm, Fq, a[i], debug)
        return A


def ext_inv(Fqm, Fq, A, debug=False):
    """ ext^(-1) for a mxl matrix over Fq. Returns an l-dim. vector over Fqm """
    
    # check for consistency
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    if not A.nrows()==m:
        raise ValueError("Matrix must have dimensions mxn, where m is the extension degree of Fqm of Fq")
    n = A.ncols()

    # compute result
    a = vector(Fqm, n)
    for i in xrange(n):
        #a[i] = map_vector_to_field(Fqm, Fq, A[:,i])
        #a[i] = map_Fq_to_Fqm(Fqm, Fq, A[:,i], debug)
        a[i] = ext_inv_single(Fqm, Fq, A[:,i], debug)
    return a















"""
Old - not really working - Code (maybe used again for testing):
"""

"""

def map_field_to_vector(Fqm, Fq, x):
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    result = Matrix(Fq, m, 1)
    if x!=Fqm(0):
        gen = Fqm.multiplicative_generator()
        Fq_gen = Fq.multiplicative_generator()
        index = x.log(gen)+1
        for i in xrange(m):
            tmp = index.mod(q)
            if tmp==0:
                result[i,0] = Fq(0)
            else:
                result[i,0] = Fq_gen^(tmp-1)
            index = (index/q).floor()
    return result

def map_vector_to_field(Fqm, Fq, x):
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    Fq_gen = Fq.multiplicative_generator()
    index = 0
    for i in xrange(m):
        if x[i]!=0:
            print x[i,0].log(Fq_gen)
            index += q^i*(x[i,0].log(Fq_gen)+1)
    print index
    if index==0:
        return Fqm(0)
    else:
        return Fqm.multiplicative_generator()^(index-1)


"""


"""

def map_Fqm_to_Fq(Fqm, Fq, x, debug=False):
    
    # check for consistency
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")

    # compute result
    if q==p: # if q=p prime, the mapping is easy
        if debug:
            print "----------------------------------------------"
            print "ext:"
            print x
            print x._vector_()
        return x._vector_()
    else:    # else, there has to be a detour over Fp
        Fq_gen = Fq.gen()
        tmp = x._vector_()
        #print "BANANE"
        l = q.log(p)
        result = vector(Fq, m)
        for i in xrange(m):
            result[i] = 0
            for j in xrange(l):
                result[i] += tmp[l*i+j]*Fq_gen^j
        #print "START"
        #print "x   = ", x
        #print "tmp = ", tmp
        #print "res = ", result
        #print "STOP"
        return result


def map_Fq_to_Fqm(Fqm, Fq, x, debug=False):
        
    # check for consistency
    p = Fq.characteristic()
    q = Fq.order()
    if p!=Fqm.characteristic():
        raise ValueError("Fqm and Fq must have the same characteristic")
    m = Fqm.order().log(q)
    if m not in ZZ:
        raise ValueError("Fqm must be a field extension of Fq")
    
    # compute result
    result = Fqm(0)
    gen = Fqm.gen()
    if q==p: # if q=p prime, the mapping is easy
        for i in xrange(m):
            result += x[i,0]*gen^i
        if debug:
            print "----------------------------------------------"
            print "ext_inv:"
            print x.str()
            print result
        return result
    else:    # else, there has to be a detour over Fp
        j = 0
        l = q.log(p)
        #print "l = ", l
        #print "START_inv"
        #print "x =   ", x.str()
        for i in xrange(m):
            tmp = x[i,0]._vector_()
            #print "tmp = ", tmp
            for k in xrange(l):
                #print l*i+k
                result += tmp[k]*gen^(l*i+k)
        #print "res = ", result
        return result


"""
