""" Some helper functions """

load maps.sage
load linearized_polynomials.sage

def minimal_subspace_polynomial(L, gs, exception=-1):
    """ Compute the minimal subspace polynomial of the Fqm-elements in the vector gs.
    If exception!=-1, skip the i-th value in gs (used to find the i-th linearized
    Lagrange basis polynomial). Computation like in Section III.B [SK07] """
    n = gs.degree()
    q = L.q
    M = LinearizedPolynomial(L, [1])
    for i in xrange(n):
        if i!=exception:
            b = M.evaluate(gs[i])
            N = LinearizedPolynomial(L, [-b^(q-1), 1])
            M = N._mul_(M)
    return M

def check_if_basis(gs, q, Fqm):
    
    m = gs.degree()
    M = matrix(Fqm, m, m)
    for i in xrange(m):
        for j in xrange(m):
            M[i,j] = gs[j]^(q^i)
    # gs is a basis of Fqm over Fq if and only if det(M)!=0
    # cf. Corollary 2.38 [LN97]
    return M.determinant()!=0



"""
BIBLIOGRAPHY

[SK07] D. Silva and F.R. Kschischang,
       Rank Metric Codes for Priority Encoding Transmission with Network Coding,
       Canadian Workshop of Inf. Theory (CWIT), Alberta, Canada, 2007

[LN97] R. Lidl and H. Niederreiter,
       Finite Fields,
       Encyclopedia of Mathematics and its Applications 20, 1997

"""







"""
Testing Code
"""

"""
# Parameters
q = 2
m = 10
k = 3

# More Variables
n = m
Fq = GF(q, 'a')
Fqm = GF(q^m, 't')

# Initialize gs 
GS = matrix(Fq, m, m)
for i in xrange(m):
    GS[i,i] = 1
#print GS
gs = ext_inv(Fqm, Fq, GS)
#print gs
#b = Fqm.multiplicative_generator()
#for i in xrange(m):
#    gs[0,i] = b^(-q^i)
#GS = ext(Fqm, Fq, gs)
#print "rank"
#print GS.rank()

# Create Linearized Polynomial Ring
L = LinearizedPolynomialRing(Fqm, q)


M = minimal_subspace_polynomial(L, gs)
print M

#M = LinearizedPolynomial(L, [-1,0,0,0,0,0,0,0,0,0,1])

#for i in xrange(10):
#    print M.evaluate(gs[0,i])

#print "APFEL"
#for a in Fqm:
#    print M.evaluate(a)

#print "BANANE"
#check_if_basis(gs,q,Fqm)

"""
